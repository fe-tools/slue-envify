# slue-envify changelog

## 1.1.0

### 环境变量替换后，格式化代码

## 1.1.1

### readme.md slue-envify require change

## 1.1.2

### slue-envify describe

## 1.1.3

### slue-envify describe

## 1.1.4

### do nothing when the file has no variables

## 1.1.5

### add useBabel param, default true. when useBabel is false, will not tranform es6

## 1.1.6

### update readme.md