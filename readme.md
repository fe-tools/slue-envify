# slue-envify

Replace process.env variables in your script and then parse the result code. Support es6.

## Usage

For example, there is a file named main.js
```javascript
const fs = require('fs');

if (process.env.NODE_ENV === 'production') {
    var vars1 = 10;
} else {
    var vars = 20;
}

if (process.env.NODE_ENV_R === 'production' && flag) {
    console.log(123);
} else {
    console.log(456);
}
```

run this script:
```javascript
const slueFs = require('slue-fs');
const slueStream = require('slue-stream');
const slueEnvify = require('slue-envify');

const envs = {
    NODE_ENV: 'development',
    NODE_ENV_R: 'production'
};
slueFs.read('./test/main.js')
    .pipe(slueEnvify({
        envs,
        useBabel: true // Is transform es6. Default true
    }))
    .pipe(slueStream.transformObj(function(file, env, cb) {
        console.log(file.contents.toString());
        cb(null, file);
    }));
```

the output is: 
```javascript
"use strict";

var vars1, fs = require("fs"), vars = 20;

flag ? console.log(123) : console.log(456);
```