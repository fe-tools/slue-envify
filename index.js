const path = require('path');
const slueStream = require('slue-stream');
const babel = require('babel-core');
const UglifyJS = require("uglify-js");

module.exports = function(opts = {}) {
    if (typeof opts.useBabel !== 'boolean') {
        opts.useBabel = true;
    }
    return slueStream.transformObj(function(file, env, cb) {
        let extname = path.extname(file.path);
        if (extname === '.js') {
            let hasVariables = false;
            let contents = file.contents.toString();
            for (let env in opts.envs) {
                let regular = new RegExp(`process\\\.env\\\.${env}[^z-zA-Z_0-9]`, 'g');
                contents = contents.replace(regular, function(item) {
                    hasVariables = true;
                    let _regular = new RegExp(`process\\\.env\\\.${env}`, 'g');
                    item = item.replace(_regular, `\'${opts.envs[env]}\'`);
                    return item;
                });
            }
            if (hasVariables) {
                if (opts.useBabel === true) {
                    try {
                        let result = babel.transform(contents, {
                            presets: [
                                require('babel-preset-env'),
                                require('babel-preset-react'),
                                require('babel-preset-stage-3')
                            ]
                        });
                        contents = result.code;
                    } catch (e) {
                        e.filePath = file.path;
                        console.log(e);
                    }
                }

                let res = UglifyJS.minify(contents, {
                    output: {
                        beautify: true
                    }
                });
                if (res.error) {
                    res.error.filePath = file.path;
                    console.log(res.error);
                } else {
                    file.contents = Buffer.from(res.code);
                }
            }

            cb(null, file);
        } else {
            cb(null, file);
        }
    });
}