const slueFs = require('slue-fs');
const slueStream = require('slue-stream');
const slueEnvify = require('../index');

const envs = {
    NODE_ENV: 'development',
    NODE_ENV_R: 'production'
};
slueFs.read('./test/main.js')
    .pipe(slueEnvify({
        envs,
        useBabel: true
    }))
    .pipe(slueStream.transformObj(function(file, env, cb) {
        console.log(file.contents.toString());
        cb(null, file);
    }));